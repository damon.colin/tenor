package com.tenor.hub.transport.domain;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.tenor.hub.UnitTest;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@UnitTest
@ExtendWith(MockitoExtension.class)
class MessageTest {

  @Mock
  private RoutesFilterRepository routes;

  @Test
  void shouldNotRouteToUnknownFilter() {
    assertThatThrownBy(() -> message().withRoute(routes)).isExactlyInstanceOf(RuntimeException.class);
  }

  @Test
  void shouldRouteEngieInvoiceToEngie() {
    when(routes.findMapping(any())).thenReturn(Optional.of(new RouteFilter(engieMapping())));

    assertThat(message().withRoute(routes)).isEqualTo(new RoutedMessage(engieMapping(), message()));
  }

  private Message message() {
    return new Message(file());
  }

  private File file() {
    return new File(new Siret("5454"), new Siren("44545"), new Filename("peut.xml"));
  }

  private Mapping engieMapping() {
    return new Mapping("engie");
  }
}
