package com.tenor.hub.transport.domain;

public record Filename(String value) {}
