package com.tenor.hub.transport.domain;

public record RouteFilter(Mapping mapping) {}
