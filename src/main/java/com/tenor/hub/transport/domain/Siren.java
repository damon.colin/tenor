package com.tenor.hub.transport.domain;

public record Siren(String value) {}
