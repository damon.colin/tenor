package com.tenor.hub.transport.domain;

import com.tenor.hub.shared.error.domain.Assert;

public record Mapping(String value) {
  public Mapping {
    Assert.field("mapping", value).notBlank().maxLength(100);
  }
}
