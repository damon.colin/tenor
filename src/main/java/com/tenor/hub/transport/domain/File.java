package com.tenor.hub.transport.domain;

public record File(Siret siret, Siren siren, Filename name) {
  RouteFilterCriteria routeCriteria() {
    return new RouteFilterCriteria(siret(), siren(), name());
  }
}
