package com.tenor.hub.transport.domain;

import com.tenor.hub.shared.error.domain.Assert;

public record Message(File file) {
  public Message {
    Assert.notNull("file", file);
  }

  public RoutedMessage withRoute(RoutesFilterRepository routes) {
    Assert.notNull("routes", routes);

    return routes
      .findMapping(file.routeCriteria())
      .map(filter -> new RoutedMessage(filter.mapping(), this))
      .orElseThrow(RuntimeException::new);
  }
}
