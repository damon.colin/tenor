package com.tenor.hub.transport.domain;

public record RouteFilterCriteria(Siret siret, Siren siren, Filename name) {}
