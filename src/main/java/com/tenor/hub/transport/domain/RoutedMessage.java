package com.tenor.hub.transport.domain;

import com.tenor.hub.shared.error.domain.Assert;

public record RoutedMessage(Mapping mapping, Message message) {
  public RoutedMessage {
    Assert.notNull("mapping", mapping);
    Assert.notNull("message", message);
  }
}
