package com.tenor.hub.transport.domain;

import java.util.Optional;

public interface RoutesFilterRepository {
  Optional<RouteFilter> findMapping(RouteFilterCriteria criteria);
}
